package com.yassine.compta.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;
@Entity
public class Client implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="CODE_CLIENT")
	private int codeClient;
	
	@javax.persistence.Temporal(TemporalType.DATE)
	private Date dateCreation;
	
	@javax.persistence.Temporal(TemporalType.DATE)
	private Date dateCloture;
	
	private String formeJuridique;
	
	@ManyToOne
	@JoinColumn
	private User user;
	
	@Column(name="NOM_CLIENT")
	private String nomClient;
	
	private String mois; 
	
//	@ColumnDefault("'0'")
//	@Column(nullable=false)
	private String tenue="0";
	
//	@ColumnDefault("'0'")
//	@Column(nullable=false)
	private String compteAnnuel="0";
	
//	@ColumnDefault("'0'")
//	@Column(nullable=false)
	private String date_transmi_CGA="0";
	
//	@ColumnDefault("'0'")
//	@Column(nullable=false)
	private String juri_depot_compte="0";
	
//	@ColumnDefault("0")
//	@Column(nullable=false)
	private String tva="0";
	
//	@ColumnDefault("0")
//	@Column(nullable=false)
	private String taxe_salaire="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String liasse="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String is="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String cve_1330="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String cve_1329="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String cfe_14_m="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String cfe_14_c="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String cfe_accompte="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String cfe_solde="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String ch_2777_d="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String ifu="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String ta="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String fpc="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String ec="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String c3s="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String tvts="0";
	
	@ColumnDefault("0")
	@Column(nullable=false)
	private String dcr="0";
	
	public String getDate_transmi_CGA() {
		return date_transmi_CGA;
	}
	public void setDate_transmi_CGA(String date_transmi_CGA) {
		this.date_transmi_CGA = date_transmi_CGA;
	}
	public String getJuri_depot_compte() {
		return juri_depot_compte;
	}
	public void setJuri_depot_compte(String juri_depot_compte) {
		this.juri_depot_compte = juri_depot_compte;
	}
	public String getTva() {
		return tva;
	}
	public void setTva(String tva) {
		this.tva = tva;
	}
	public String getTaxe_salaire() {
		return taxe_salaire;
	}
	public void setTaxe_salaire(String taxe_salaire) {
		this.taxe_salaire = taxe_salaire;
	}
	public String getLiasse() {
		return liasse;
	}
	public void setLiasse(String liasse) {
		this.liasse = liasse;
	}
	public String getIs() {
		return is;
	}
	public void setIs(String is) {
		this.is = is;
	}
	public String getCve_1330() {
		return cve_1330;
	}
	public void setCve_1330(String cve_1330) {
		this.cve_1330 = cve_1330;
	}
	public String getCve_1329() {
		return cve_1329;
	}
	public void setCve_1329(String cve_1329) {
		this.cve_1329 = cve_1329;
	}
	public String getCfe_14_m() {
		return cfe_14_m;
	}
	public void setCfe_14_m(String cfe_14_m) {
		this.cfe_14_m = cfe_14_m;
	}
	public String getCfe_14_c() {
		return cfe_14_c;
	}
	public void setCfe_14_c(String cfe_14_c) {
		this.cfe_14_c = cfe_14_c;
	}
	public String getCfe_accompte() {
		return cfe_accompte;
	}
	public void setCfe_accompte(String cfe_accompte) {
		this.cfe_accompte = cfe_accompte;
	}
	public String getCfe_solde() {
		return cfe_solde;
	}
	public void setCfe_solde(String cfe_solde) {
		this.cfe_solde = cfe_solde;
	}
	public String getCh_2777_d() {
		return ch_2777_d;
	}
	public void setCh_2777_d(String ch_2777_d) {
		this.ch_2777_d = ch_2777_d;
	}
	public String getIfu() {
		return ifu;
	}
	public void setIfu(String ifu) {
		this.ifu = ifu;
	}
	public String getTa() {
		return ta;
	}
	public void setTa(String ta) {
		this.ta = ta;
	}
	public String getFpc() {
		return fpc;
	}
	public void setFpc(String fpc) {
		this.fpc = fpc;
	}
	public String getEc() {
		return ec;
	}
	public void setEc(String ec) {
		this.ec = ec;
	}
	public String getC3s() {
		return c3s;
	}
	public void setC3s(String c3s) {
		this.c3s = c3s;
	}
	public String getTvts() {
		return tvts;
	}
	public void setTvts(String tvts) {
		this.tvts = tvts;
	}
	public String getDcr() {
		return dcr;
	}
	public void setDcr(String dcr) {
		this.dcr = dcr;
	}
	
	public int getCodeClient() {
		return codeClient;
	}
	public void setCodeClient(int codeClient) {
		this.codeClient = codeClient;
	}
	public Date getDateCloture() {
		return dateCloture;
	}
	public void setDateCloture(Date dateCloture) {
		this.dateCloture = dateCloture;
	}
	public String getFormeJuridique() {
		return formeJuridique;
	}
	public void setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Client(int codeClient, String nomC, Date date2, String form) {
		super();
		this.codeClient = codeClient;
		this.nomClient = nomC;
		this.formeJuridique = form;
		this.dateCloture = date2;
	}
	
	@Override
	public String toString() {
		return "Client [codeClient=" + codeClient + ", dateCloture=" + dateCloture + ", formeJuridique="
				+ formeJuridique + ", user=" + user + "]";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNomClient() {
		return nomClient;
	}
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}
	
	public String getTenue() {
		return tenue;
	}
	public void setTenue(String tenue) {
		this.tenue = tenue;
	}
	public String getMois() {
		return mois;
	}
	public void setMois(String mois) {
		this.mois = mois;
	}
	public String getCompteAnnuel() {
		return compteAnnuel;
	}
	public void setCompteAnnuel(String compteAnnuel) {
		this.compteAnnuel = compteAnnuel;
	}
	
	public Client(int codeClient, String nomClient, String tenue, String compteAnnuel, String dateTransmissionCGA,
			String juridiqueDepotCompte, String tva, String taxeSalaire, String liasse, String iS, String cVAE_1330,
			String cVAE_1329, String cFE_1447, String cFE_1447_C, String cFE_Acompte, String cFE_Solde, String n_2777_D,
			String iFU, String tA, String fPC, String eC, String c3s, String tVTS, Date dateCloture,
			String formeJuridique, User user) {
		super();
		this.codeClient = codeClient;
		this.nomClient = nomClient;
		this.tenue = tenue;
		this.compteAnnuel = compteAnnuel;
		this.dateCloture = dateCloture;
		this.formeJuridique = formeJuridique;
		this.user = user;
	}
	public Client(int codeClient2, String nomClient2, String tenue2, Date dateCloture2, String formeJuridique2,
			Object object) {
		this.codeClient = codeClient2;
		this.nomClient = nomClient2;
		tenue = tenue2;
		this.formeJuridique = formeJuridique2;
	}
	public Client(int codeClient2, String nomClient2, Date dateCloture2, String formeJuridique2, Object object) {
		this.codeClient = codeClient2;
		this.nomClient = nomClient2;
		this.formeJuridique = formeJuridique2;
	} 
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	
}
