package com.yassine.compta.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yassine.compta.exception.ResourceNotFoundException;
import com.yassine.compta.models.Client;
import com.yassine.compta.models.User;
import com.yassine.compta.repository.ClientRepository;
import com.yassine.compta.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/all")
	public String allAccess() {
		return "Compta/Expertise";
	}
	
	@GetMapping("/user/{id}/{mois}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public List<Client> getClientsByUserAndMonth(@PathVariable String mois,@PathVariable Long id) throws ResourceNotFoundException {
		if(id==null)
			return null;
		List<Client> clients = clientRepository.findClientByIdAndMonth(id,mois);
		return clients;
	} 
	
	@GetMapping("/clients/{mois}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Client> getUtilisateurByMonth(@PathVariable String mois) throws ResourceNotFoundException {
		if(mois==null)
			return null;
		List<Client> clients = clientRepository.findClientByMonth(mois);
		return clients;
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Client> getAllCustomers() {
		return clientRepository.findAll();
	}
	
	@RequestMapping(path = "/client/{id}", method = RequestMethod.GET)
	//@PreAuthorize("hasRole(USER)")
	public ResponseEntity<Client> getClient(@PathVariable Long id) throws ResourceNotFoundException{
			Client client = clientRepository.findById(id)
			          .orElseThrow(() -> new ResourceNotFoundException("Client not found for this id :: " + id));
			        return ResponseEntity.ok().body(client);
		
	}
	
	@RequestMapping(value = "/client", method = RequestMethod.POST)
	// @PreAuthorize("hasRole(ROLE_USER)") klho
	public Client saveClient(@RequestBody Client c) {
		Client cl = new Client();
		org.springframework.security.core.Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = userRepository.findByUsername(authentication.getName());
		Client client = new Client(c.getCodeClient(), c.getNomClient(), c.getDateCloture(), c.getFormeJuridique());

		if (user == null)
			return null;
		client.setMois(c.getMois());
		client.setUser(user);
		try {
			cl = clientRepository.save(client);
		} catch (Exception e) {
			e.getMessage();
		}
		return cl;
	}
	
	@RequestMapping(path = "/client/{id}", method = RequestMethod.PUT)
	//@PreAuthorize("hasRole(ROLE_USER) or hasRole(ROLE_ADMIN)")
	public Client updateClient(@RequestBody Client c, @PathVariable Long id) {
		Client cl = new Client();
		if (id == null)
			return null;
		c.setId(id);
		try {
			cl = clientRepository.save(c);
		} catch (Exception e) {
			e.getMessage();
		}
		return cl;
	}
	
	@RequestMapping(path="/client/{id}", method=RequestMethod.DELETE)
	//@PreAuthorize("hasRole(ROLE_ADMIN)")
	public void suuprimerClient(@PathVariable Long id) {
		clientRepository.deleteById(id);
	}
}
