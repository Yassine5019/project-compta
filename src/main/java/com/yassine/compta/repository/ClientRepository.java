package com.yassine.compta.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.yassine.compta.models.Client;
public interface ClientRepository extends JpaRepository<Client, Long> {
	@Query("select c from Client c where c.codeClient like :x")
	Page<Client> chercher(@org.springframework.data.repository.query.Param("x")String mc, org.springframework.data.domain.Pageable pageable);
    
	@Query(value="FROM Client c WHERE c.user.id like :id and c.mois like :mois")
	List<Client> findClientByIdAndMonth(@org.springframework.data.repository.query.Param("id")Long id, 
			                                   @org.springframework.data.repository.query.Param("mois")String mois);
    
	@Query(value="FROM Client c WHERE c.mois like :mois")
	List<Client> findClientByMonth(@org.springframework.data.repository.query.Param("mois")String mois);
}
